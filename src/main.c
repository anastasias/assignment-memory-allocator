#include "mem.h"
#include "util.h"
#include "mem_internals.h"





#define INITIAL_HEAP_SIZE 10000

static void *test_heap_init(){
    void *heap = heap_init(INITIAL_HEAP_SIZE);
    if (heap == NULL) {
        printf( "Failed to initialize the heap");
    }
    return heap;
}




/*Обычное успешное выделение памяти */

void test1(struct block_header *first_block){
    printf( "Test 1 is starting\n");

    void *memory = _malloc(1000);
    if (memory == NULL) {
        printf("Test 1 failed: malloc return NULL\n");
    }

    debug_heap(stdout, first_block);

    if (first_block->capacity.bytes != 1000)  {
        printf("Test 1 failed: invalid block size \n");
    }
    if(first_block->is_free != false){
        printf("Test 1 failed: block is not free\n");
    }
    printf( "Test 1 passed\n");
    _free(memory);
}


/*Освобождение одного блока из нескольких выделенных. */
void test2(struct block_header *first_block){
    printf("Test 2 is starting\n");
    void *data1 = _malloc(500), *data2 = _malloc(900);
    if (!data1 || !data2)
        printf("Test 2 failed: malloc returned NULL\n");

    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data_block1 = block_get_header(data1), *data_block2 = block_get_header(data2);
    if (!data_block1->is_free)
        printf("Test 2 failed: The first block is not free\n");

    if (data_block2->is_free)
        printf("Test 2 failed: The second block is free\n");

    printf("Test 2 passed\n\n");
    _free(data1);
    _free(data2);
}


/*Освобождение двух блоков из нескольких выделенных*/
void test3(struct block_header *first_block){
    fprintf(stderr, "Test 3 is starting...\n");

    void *memory1 = _malloc(2000);
    void *memory2 = _malloc(1000);
    void *memory3 = _malloc(500);

    if (memory1 == NULL || memory2 == NULL || memory3 == NULL) {
        printf("Test 3 failed: malloc returned NULL\n");
    }

    _free(memory2);
    _free(memory1);

    debug_heap(stdout, first_block);

    struct block_header *header1 = block_get_header(memory1);
    struct block_header *header2 = block_get_header(memory2);
    struct block_header *header3 = block_get_header(memory3);

    if (!header1->is_free) {
        printf("Test 3 failed: Failed to make free the first block");
    }
    if (!header2->is_free) {
        printf("Test 3 failed: Failed to make free the second block");
    }
    if (header3->is_free) {
        printf("Test 3 failed: The second block became free, but we didn't want that");
    }
    if (header1->capacity.bytes != 3000 + offsetof(struct block_header, contents)) {
        printf("Test 3 failed: invalid block size");
    }

    printf("Test 3 passed\n");
    _free(memory3);
}



/*Память закончилась, новый регион памяти расширяет старый*/
void test4(struct block_header *first_block) {
    printf("Test 4 is starting\n");
    void *memory1 = _malloc(10000);
    void *memory2 = _malloc(10000);


    debug_heap(stdout, first_block);
    struct block_header *header1 = block_get_header(memory1);
    struct block_header *header2 =block_get_header(memory2);

    if ((uint8_t *) header1->contents + header2->capacity.bytes != (uint8_t *) header2) {
        printf("Test 4 failed\n");
    }

    _free(memory1);
    _free(memory2);
    printf("Test 4 passed\n");

}



/*Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
*/
void test5(struct block_header *first_block){
    printf("Test 5 is starting\n");


    struct block_header* last_block = first_block;
    debug_heap(stdout, first_block);
    while(last_block->next != NULL) {
        last_block = last_block->next;
    }
    debug_heap(stdout, first_block);
    map_pages((uint8_t *) last_block + size_from_capacity(last_block->capacity).bytes, 1000, MAP_FIXED);
    debug_heap(stdout, first_block);
    void* memory = _malloc(40000);
    debug_heap(stdout, first_block);
    struct block_header* block = block_get_header(memory);
    if (block == last_block) {
        printf("Test 5 failed\n");

    }
    _free(memory);
    debug_heap(stdout, first_block);
    printf("Test 5 passed\n");

}



int main(){
    struct block_header *first_block = (struct block_header *) test_heap_init();
    test1(first_block);
    test2(first_block);
    test3(first_block);
    test4(first_block);
    test5(first_block);

}
